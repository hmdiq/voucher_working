﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using Highsoft.Web.Mvc.Charts;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.ActiveDirectory.GraphClient;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using VoucherSystem.Interfaces;
using VoucherSystem.Models;
using VoucherSystem.ViewModels;

namespace VoucherSystem.Controllers
{
    //[Authorize]
    public class HomeController : Controller
    {
        private const string TenantName = "ticketfortb2c.onmicrosoft.com";
        private const string AppId = "bacbf0f0-e4ee-4c13-a7e7-d7d144a9a1cf";
        private const string AppSecretKey = "W1kOAXVDyk0Cz1XRjfavLKqF8bsGvYR/jd5QfLIKORc= ";
        private const string AadGraphUri = "https://graph.windows.net";
        private const string Authority = "https://login.microsoftonline.com/ticketfortb2c.onmicrosoft.com";
        public static string AccessToken;

        public ActiveDirectoryClient AadClient;
        public const string CartSessionKey = "Id";
        private readonly IEmailSender _emailSender;
        private readonly IApiService _apiService;
        private readonly ILogger<HomeController> _logger;
        private readonly IHostingEnvironment _hostingEnvironment;

        public HomeController(IEmailSender emailSender, IApiService apiService, ILogger<HomeController> logger, IHostingEnvironment hostingEnvironment)
        {

            _emailSender = emailSender;
            _apiService = apiService;
            _logger = logger;
            _hostingEnvironment = hostingEnvironment;

            var authContext = new AuthenticationContext(Authority);
            var clientCredential = new ClientCredential(AppId, AppSecretKey);

            var graphUri = new Uri(AadGraphUri);
            var serviceRoot = new Uri(graphUri, TenantName);
            AadClient = new ActiveDirectoryClient(serviceRoot, async () => await AcquireGraphApiAccessToken(AadGraphUri, authContext, clientCredential));


        }

        public async Task<IActionResult> Index()
        {
            try
            {
                List<Category> allCategories = await _apiService.GetAllCategories();
                return View(allCategories);
            }
            catch (Exception e)
            {
                //log exception here
                _logger.LogError("Exception Getting Categories:" + e.Message);
                return View();
            }
        }

       
        public async Task<IActionResult> Sales()
        {
            try
            {
                List<Sale> allSales= await _apiService.GetAllSales();


                var grouped = allSales.GroupBy(s => s.Category).Select(s => s.ToList()).ToList();

                List<PieSeriesData> salesData = new List<PieSeriesData>();
                List<ColumnSeriesData> revenueData = new List<ColumnSeriesData>();
                List<String> categories = new List<string>();


                grouped.ForEach(p => salesData.Add(new PieSeriesData { Y = p.Sum(c => c.Quantity),Name = p.First().Category,Sliced = true}));
                grouped.ForEach(p => revenueData.Add(new ColumnSeriesData { Y = p.Sum(c => c.SalesTotal), Name = p.First().Category}));
                grouped.ForEach(p => categories.Add(p.First().Category));

                ViewData["salesData"] = salesData;
                ViewData["revenueData"] = revenueData;
                ViewData["categories"] = categories;
                ViewData["TotalRevenue"] = allSales.Sum(c => c.SalesTotal);

                return View(allSales);
            }
            catch (Exception e)
            {
                //log exception here
                _logger.LogError("Exception Getting Categories:" + e.Message);
                return View();
            }
        }


        public IActionResult CreateUser()
        {
            return View();
        }


        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }


        public async Task ExportSales(string timeStamp)
        {
            var sales = await _apiService.GetAllSales();
            var records = new List<Sale>();
            var path = Path.Combine(_hostingEnvironment.ContentRootPath, "files/");

            foreach (var sale in sales)
            {
                //first get the created on
                //get only the date from the created on
                var date = sale.CreatedOn.ToShortDateString();

                //append that date to the time stamp

                var fullTimeStamp = date.ToString(CultureInfo.InvariantCulture) + " " + sale.TimeStamp;


                //use the new time stamp in the loop

                if (DateTime.ParseExact(fullTimeStamp, "M/d/yyyy h:mm tt", CultureInfo.InvariantCulture) > DateTime.ParseExact("12/09/2017 6:09 PM", "M/d/yyyy h:mm tt", CultureInfo.InvariantCulture))
                {
                    records.Add(sale);
                }

            }

            using (TextWriter textWriter = System.IO.File.CreateText(path+"cvsrecord.csv"))
            {
                var csv = new CsvWriter(textWriter);
                csv.WriteRecords(records);
            }

        }

        private static async Task<string> AcquireGraphApiAccessToken(string graphApiUrl, AuthenticationContext authContext, ClientCredential clientCredential)
        {
            AuthenticationResult result = null;
            var retryCount = 0;
            bool retry;
            do
            {
                retry = false;
                try
                {
                    result = await authContext.AcquireTokenAsync(graphApiUrl, clientCredential);
                    AccessToken = result.AccessToken;
                }
                catch (AdalException ex)
                {
                    if (ex.ErrorCode != "temporarily_unavailable") continue;
                    retry = true;
                    retryCount++;
                    await Task.Delay(3000);
                }
            } while (retry && (retryCount < 3));

            return result?.AccessToken;
        }
    }
}
