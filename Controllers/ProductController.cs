﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using VoucherSystem.Interfaces;
using VoucherSystem.Models;
using VoucherSystem.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VoucherSystem.Controllers
{
    //[Authorize]
    public class ProductController : Controller
    {
        private readonly IApiService _apiService;
        private readonly ILogger<ProductController> _logger;
        private readonly IImageHelper _imageHelper;

        public ProductController (IApiService apiService, ILogger<ProductController> logger, IImageHelper imageHelper)
        {
            _apiService = apiService;
            _logger = logger;
            _imageHelper = imageHelper;
        }

        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            try
            {
                List<Category> allCategories = await _apiService.GetAllCategories();
                return View(allCategories);
            }
            catch (Exception e)
            {
                //log exception here
                _logger.LogError("Exception Getting Categories:" + e.Message);
                return View();
            }
         
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ProductViewModel product)
        {
            if (!ModelState.IsValid)
            {
                //set error message in view bag here
                return View(product);
            }

            try
            {
                Category category = new Category
                {
                    Name = product.Name,
                    Price = product.Price,
                    Quantity = product.Quantity,
                    ImageUrl = _imageHelper.UploadCategoryImageToCdnAsync(product.Image).Result
                };
                //call api service here to make request
                var res = await _apiService.CreateCategory(category);

                if (res.Status)
                {
                    return RedirectToAction("Index");
                }


                //log error message here
                _logger.LogError("Error Calling create product api:" + res.Message);
                return View(product);
            }
            catch (Exception ex)
            {
                //log exception here
                _logger.LogError("Exception Creating Product:"+ ex.Message);
                return View(product);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Update(int id)
        {
            if (id < 0)
            {
                return View("Index");
            }
            else
            {
                var category = await _apiService.GetCategory(id);
                return View(category);
            }
          
        }

        [HttpPost]
        public async Task<IActionResult> Update(Category category)
        {
            if (!ModelState.IsValid)
            {
                //set error message in view bag here
                return View(category);
            }

            try
            {
              
                //call api service here to make request
                var res = await _apiService.UpdateCategory(category);

                if (res.Status)
                {
                    return RedirectToAction("Index");
                }


                //log error message here
                _logger.LogError("Error Calling update category api:" + res.Message);
                return View(category);
            }
            catch (Exception ex)
            {
                //log exception here
                _logger.LogError("Exception Updating Product:" + ex.Message);
                return View(category);
            }
        }

        [HttpPost]
        public async Task<IActionResult> UpdateCategoryImage(IFormCollection files)
        {
            if (files == null)
            {
                //set error message in view bag here
                return BadRequest(new{error="Image File is empty!"});
            }

            try
            {
                UpdateCategoryImageModel model = new UpdateCategoryImageModel
                {
                    Id= Convert.ToInt32(files["id"]),
                    ImgUrl = _imageHelper.UploadCategoryImageToCdnAsync(files.Files[0]).Result
                }; 

                var response = await _apiService.UpdateCategoryImage(model);

                if (response.Status)
                {
                    return View("Index");
                }

                //log error message here
                _logger.LogError("Error Calling update category image api:" + response.Message);
                return RedirectToAction("Update",new{id = Convert.ToInt32(files["id"]) });
            }
            catch (Exception ex)
            {
                //log exception here
                _logger.LogError("Exception Updating Product:" + ex.Message);
                return RedirectToAction("Update", new { id = Convert.ToInt32(files["id"]) });
            }
        }

        public async Task<IActionResult> PrintVoucher(PrinterObject[] selections)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(new { error = "Data is invalid" });
            }

            //retrieve system environment
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var isDevelopment = environment == EnvironmentName.Development;

            var loggedInUser = isDevelopment ? "Demo" : User.Claims.FirstOrDefault(c => c.Type.Equals("emails"))?.Value;

            //log sale here
            //for each item in selections create a sales log
            List<Sale> sales = selections.ToList()
                .Select(selection => new Sale
                {
                    CategoryId = selection.Id,
                    Quantity = selection.Quantity,
                    SalesPerson = loggedInUser,
                    SalesTotal = selection.Quantity * selection.Price,
                    TimeStamp = DateTime.Now.ToShortTimeString()
                })
                .ToList();


            //send the create requests
            var response = await _apiService.CreateSalesLog(sales);

            if (response.Status)
            {
                List<UpdateCategoryModel> itemsToUpdateList = selections.ToList()
                    .Select(item => new UpdateCategoryModel
                    {
                        Id = item.Id,
                        Quantity = item.Quantity
                    })
                    .ToList();

                //perform database update

                var updateResponse = await _apiService.ReduceCategoryQuantity(itemsToUpdateList);

                if (updateResponse.Status)
                {
                    //perform print here
                    List<PrinterObject> printerObjects = new List<PrinterObject>();

                    var selectionsAsList = selections.ToList();

                    foreach (var selection in selectionsAsList)
                    {
                        for (var i = 0; i < selection.Quantity; i++)
                        {
                            printerObjects.Add(new PrinterObject
                            {
                                Product = selection.Product,
                                Quantity = selection.Quantity,
                                Price = selection.Price
                                
                            });
                        }
                    }
                    //return ok

                    ViewData["date"] = DateTime.Now.ToShortDateString();
                    ViewData["time"] = DateTime.Now.ToShortTimeString();

                    return PartialView("VoucherTemplatePartialView", printerObjects);
                }
                return BadRequest(new { error = "Data is invalid" });
            }
            return BadRequest(new { error = response.Message });
        }  
    }
}
