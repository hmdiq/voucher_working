﻿
using System.ComponentModel.DataAnnotations;

namespace VoucherSystem.ViewModels
{
    public class UserVm
    {
        [DataType(DataType.Text)]
        [StringLength(100,ErrorMessage = "Company name cannot be longer that 100 characters.")]
        [Required]
        [Display(Name = "Company Name")]
        public string Name { get; set; }

        [DataType(DataType.EmailAddress)]
        [Required]
        [Display(Name = "Company Email")]
        public string Email { get; set; }

        [DataType(DataType.EmailAddress)]
        [Required]
        [Display(Name = "Confirm Email")]
        [Compare("Email", ErrorMessage = "The emails do not match.")]
        public string ConfirmEmail { get; set; }

        [DataType(DataType.Password)]
        [Required]
        [Display(Name = "Password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Required]
        [Display(Name = "Phone Number")]
        public string Phone { get; set; }

    }
}
