﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace VoucherSystem.ViewModels
{
    public class ProductViewModel
    {
        [Required]
        [DataType(DataType.Text,ErrorMessage = "Please enter a name for your product.")]
        [StringLength(100, MinimumLength = 3)]
        public string Name { get; set; }

        [Required]
        [DisplayName("Price")]
        public int Price { get; set; }

        [Required]
        [DisplayName("Quantity")]
        public int Quantity { get; set; }

        [Required]
        [DataType(DataType.Upload, ErrorMessage = "Please upload an image for your product.")]
        [Display(Name = "Product Image")]      
        public IFormFile Image { get; set; }
    }
}
