﻿// Write your JavaScript code.

function PrintVouchers()
{
    var printSet = [];
    var total = 0;

    $("input[type=checkbox]:checked").each(function ()
    {

        var id = $(this).prop("name");

        printSet.push(
            {
                "Quantity": $(this).val(),
                "Product": id,
                "Id": $(this).parent().prop("id"),
                "Price": $(this).parent().parent().prop("id")
            });
        total = total + (parseInt($(this).parent().parent().prop("id")) * parseInt($(this).val()));

    });

    $.confirm({
        title: 'Confirm Selection!',
        content: '<div>' +
                    '<ol id="itemList">' +
                    '</ol>' +
                '</div>' +
        '    <div>' +
                    '<h2>' +
        'Total is: &#8358; ' + total.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")
                    + '</h2> ' +
                '</div>',
        type: 'blue',
        typeAnimated: true,
        buttons: {
            Print: {
                btnClass: 'btn-blue',
                action: function ()
                {
                    $.ajax({
                        url: "/Product/PrintVoucher",
                        type: "POST",
                        data: { selections: printSet },
                        success: function (result)
                        {
                            $('#print-target').replaceWith(result);
                            DialogVoucherPrint();
                        },
                        error: function (error)
                        {
                            toastr.error("Error!:" + error.error);
                            toastr.options.closeButton = true;
                            toastr.options.timeOut = 60; // How long the toast will display without user interaction
                        }
                    });    
                }
            }             
            ,
            Cancel:{
                btnClass: 'btn-red',
                action: function () { }
            }
        },
        onContentReady: function ()
        {

            $.each(printSet, function (i) {
                $("#itemList").append("<li> You Have Selected: " + printSet[i].Quantity + " " + printSet[i].Product + " Voucher(s)</li>");
            });
        }
    });

}

$(function () {
    $('.button-checkbox').each(function () {

        // Settings
        var $widget = $(this),
            $button = $widget.find('button'),
            $checkbox = $widget.find('input:checkbox'),
            color = $button.data('color'),
            settings = {
                on: {
                    icon: 'glyphicon glyphicon-check'
                },
                off: {
                    icon: 'glyphicon glyphicon-unchecked'
                }
            };

        // Event Handlers
        $button.on('click', function () {
            $checkbox.prop('checked', !$checkbox.is(':checked'));
            $checkbox.triggerHandler('change');
            updateDisplay();
        });
        $checkbox.on('change', function () {
            updateDisplay();
        });

        // Actions
        function updateDisplay() {
            var isChecked = $checkbox.is(':checked');

            // Set the button's state
            $button.data('state', (isChecked) ? "on" : "off");

            // Set the button's icon
            $button.find('.state-icon')
                .removeClass()
                .addClass('state-icon ' + settings[$button.data('state')].icon);

            // Update the button's color
            if (isChecked) {
                $button
                    .removeClass('btn-default')
                    .addClass('btn-' + color + ' active');
            }
            else {
                $button
                    .removeClass('btn-' + color + ' active')
                    .addClass('btn-default');
            }
        }

        // Initialization
        function init() {

            updateDisplay();

            // Inject the icon if applicable
            if ($button.find('.state-icon').length == 0) {
                $button.prepend('<i class="state-icon ' + settings[$button.data('state')].icon + '"></i> ');
            }
        }
        init();
    });
});

function DialogVoucherPrint()
{
    var dialogVoucherPrintContent = document.getElementById('print-output');
    var dialogVoucherPrintWindow = window.open('Print Window');
    dialogVoucherPrintWindow.document.write(dialogVoucherPrintContent.innerHTML);
    //dialogVoucherPrintWindow.document.getElementById('hidden_div').style.display = 'block';
    dialogVoucherPrintWindow.document.close();
    dialogVoucherPrintWindow.focus();
    dialogVoucherPrintWindow.print();
    dialogVoucherPrintWindow.close();
    setTimeout(function () { dialogVoucherPrintWindow.close(); }, 10);

    window.location = "/Home/Index";

    return false;
}


var ticketImage;

function previewFile(fileInput) {
    if (fileInput.files && fileInput.files[0]) {
        ticketImage = fileInput.files[0];
        var reader = new FileReader();
        reader.onloadend = function (e) {
            $("#imgPreview").attr("src", e.target.result);
        }

        reader.readAsDataURL(fileInput.files[0]);
    }
}

function UpdateCategoryImage(Id)
{
    var formData = new FormData();

    formData.append("id", Id);
    formData.append("image", ticketImage);


    $.ajax({
        url: "/Product/UpdateCategoryImage",
        type: "POST",
        dataType: "json",
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {

            toastr.success("Image updated!");
            toastr.options.closeButton = true;
            window.location = "/Product/Index";
        },
        error: function (error)
        {
            toastr.error("Error!:" +error.error);
            toastr.options.closeButton = true;
        }
    });
}


function EnablePrintButton() {
   document.getElementById('printButton').disabled = false;
   
}