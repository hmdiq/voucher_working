﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using VoucherSystem.Interfaces;

namespace VoucherSystem.Services
{
    public class ImageHelper:IImageHelper
    {
        private IConfiguration Configuration { get; set; }
        private readonly ILogger<ImageHelper> _logger;

        private const string CdnPathForCategories = "https://ticketfort.azureedge.net/images/categories/";

        public ImageHelper(IConfiguration configuration, ILogger<ImageHelper> logger)
        {
            Configuration = configuration;
            _logger = logger;
        }

        public async Task<string> UploadCategoryImageToCdnAsync(IFormFile imageFile)
        {
            try
            {
                var storageAccount = CloudStorageAccount.Parse(Configuration["StorageConnectionString"]);
                var blobClient = storageAccount.CreateCloudBlobClient();
                var container = blobClient.GetContainerReference("images/categories");
                var blockBlob = container.GetBlockBlobReference(imageFile.FileName);

                using (var fileStream = imageFile.OpenReadStream())
                {
                   await blockBlob.UploadFromStreamAsync(fileStream);
                 
                }

                return CdnPathForCategories + imageFile.FileName;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error pushing file to cdn: " + ex);
                return null;
            }
        }
    }
}
