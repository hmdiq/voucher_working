﻿using System;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using VoucherSystem.Interfaces;

namespace VoucherSystem.Services
{
    public class AzureServicesHelper: IAzureServicesHelper
    {
        private IConfiguration Configuration { get;}
        private readonly ILogger<AzureServicesHelper> _logger;

        private const string CdnPathTickets = "https://ticketfort.azureedge.net/images/tickets/";
        private const string CdnPathArtists = "https://ticketfort.azureedge.net/images/artists/";
        private const string CdnPathEvents = "https://ticketfort.azureedge.net/images/events/";

        public AzureServicesHelper(IConfiguration configuration, ILogger<AzureServicesHelper> logger)
        {
            Configuration = configuration;
            _logger = logger;
        }

        public byte[] GetByteArrayFromImage(IFormFile file)
        {
            if (file.Length <= 0) return null;
            using (var fileStream = file.OpenReadStream())
            using (var ms = new MemoryStream())
            {
                fileStream.CopyTo(ms);
                var fileBytes = ms.ToArray();
                return fileBytes;
            }
        }

        public string UploadEventImageToCdn(IFormFile imageFile)
        {
            try
            {
                var storageAccount = CloudStorageAccount.Parse(Configuration["StorageConnectionString"]);
                var blobClient = storageAccount.CreateCloudBlobClient();
                var container = blobClient.GetContainerReference("images/events");
                var blockBlob = container.GetBlockBlobReference(imageFile.FileName);

                using (var fileStream = imageFile.OpenReadStream())
                {
                    blockBlob.UploadFromStreamAsync(fileStream);
                }

                return CdnPathEvents + imageFile.FileName;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error pushing file to cdn: " + ex);
                return null;
            }
        }

        public string UploadTicketImageToCdn(IFormFile imageFile)
        {
            try
            {
                var storageAccount = CloudStorageAccount.Parse(Configuration["StorageConnectionString"]);
                var blobClient = storageAccount.CreateCloudBlobClient();
                var container = blobClient.GetContainerReference("images/tickets");
                var blockBlob = container.GetBlockBlobReference(imageFile.FileName);

                using (var fileStream = imageFile.OpenReadStream())
                {
                    blockBlob.UploadFromStreamAsync(fileStream);
                }

                return CdnPathTickets + imageFile.FileName;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error pushing file to cdn: " + ex);
                return null;
            }
        }

        public string UploadArtistImageToCdn(IFormFile imageFile)
        {
            try
            {
                var storageAccount = CloudStorageAccount.Parse(Configuration["StorageConnectionString"]);
                var blobClient = storageAccount.CreateCloudBlobClient();
                var container = blobClient.GetContainerReference("images/artists");
                var blockBlob = container.GetBlockBlobReference(imageFile.FileName);

                using (var fileStream = imageFile.OpenReadStream())
                {
                    blockBlob.UploadFromStreamAsync(fileStream);
                }

                return CdnPathArtists + imageFile.FileName;
            }
            catch (Exception ex)
            {
                _logger.LogError("Error pushing file to cdn: " + ex);
                return null;
            }
        }
    }
}
