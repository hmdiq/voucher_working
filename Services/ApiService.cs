﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using VoucherSystem.Interfaces;
using VoucherSystem.Models;

namespace VoucherSystem.Services
{
    public class ApiService:IApiService
    {
        //public static string WebApiDevUrl = "http://localhost:50124/";
        public static string WebApiDevUrl = "http://voucherapi.azurewebsites.net/";

        public ApiService()
        {
            
        }


        public async Task<MethodResponse> CreateCategory(Category category)
        {
            string url = WebApiDevUrl + "api/voucher/createcategory";

            try
            {

                using (var hc = new HttpClient())
                {
                    try
                    {
                        hc.DefaultRequestHeaders.Accept.Clear();
                        hc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        //hc.DefaultRequestHeaders.Authorization =
                        //    new AuthenticationHeaderValue("Bearer", "demo");


                        var content = new StringContent(JsonConvert.SerializeObject(category),Encoding.UTF8, "application/json");
                        var response = await hc.PostAsync(url, content);
                        if (response.IsSuccessStatusCode)
                        {
                            var responseContent = await response.Content.ReadAsStringAsync();
                            var postOutput = JsonConvert.DeserializeObject<MethodResponse>(responseContent);

                            return postOutput;
                        }
                    }
                    catch (Exception exception)
                    {
                        return new MethodResponse
                        {
                            Status = false,
                            Message = exception.Message
                        };
                    }
                    return new MethodResponse
                    {
                        Status = false,
                        Message = "Error in using method"
                    };
                }
            }
            catch (Exception e)
            {
                return new MethodResponse
                {
                    Status = false,
                    Message = e.Message
                };
            }
        }

        public async Task<List<Category>> GetAllCategories()
        {
            string url = WebApiDevUrl + "api/voucher/getallcategories";

            try
            {

                using (var hc = new HttpClient())
                {
                    try
                    {
                        var response = await hc.GetAsync(url);
                        if (response.IsSuccessStatusCode)
                        {
                            var responseContent = await response.Content.ReadAsStringAsync();
                            var postOutput = JsonConvert.DeserializeObject<List<Category>>(responseContent);

                            return postOutput;
                        }
                    }
                    catch (Exception exception)
                    {
                        return new List<Category>();
                    }
                    return new List<Category>();
                }
            }
            catch (Exception e)
            {
                return new List<Category>();
            }
        }

        public async Task<MethodResponse> CreateSalesLog(List<Sale> sales)
        {
            string url = WebApiDevUrl + "api/voucher/logsale";

            try
            {

                using (var hc = new HttpClient())
                {
                    try
                    {
                        hc.DefaultRequestHeaders.Accept.Clear();
                        hc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        //hc.DefaultRequestHeaders.Authorization =
                        //    new AuthenticationHeaderValue("Bearer", "demo");


                        var content = new StringContent(JsonConvert.SerializeObject(sales), Encoding.UTF8, "application/json");
                        var response = await hc.PostAsync(url, content);
                        if (response.IsSuccessStatusCode)
                        {
                            var responseContent = await response.Content.ReadAsStringAsync();
                            var postOutput = JsonConvert.DeserializeObject<MethodResponse>(responseContent);

                            return postOutput;
                        }
                    }
                    catch (Exception exception)
                    {
                        return new MethodResponse
                        {
                            Status = false,
                            Message = exception.Message
                        };
                    }
                    return new MethodResponse
                    {
                        Status = false,
                        Message = "Error in using method"
                    };
                }
            }
            catch (Exception e)
            {
                return new MethodResponse
                {
                    Status = false,
                    Message = e.Message
                };
            }
        }

        public async Task<Category> GetCategory(int categoryId)
        {
            string url = WebApiDevUrl + "api/voucher/getcategory/?id=" + categoryId;

            try
            {

                using (var hc = new HttpClient())
                {
                    try
                    {
                        var response = await hc.GetAsync(url);
                        if (response.IsSuccessStatusCode)
                        {
                            var responseContent = await response.Content.ReadAsStringAsync();
                            var postOutput = JsonConvert.DeserializeObject<Category>(responseContent);

                            return postOutput;
                        }
                    }
                    catch (Exception exception)
                    {
                        return new Category();
                    }
                    return new Category();
                }
            }
            catch (Exception e)
            {
                return new Category();
            }
        }

        public async Task<MethodResponse> ReduceCategoryQuantity(List<UpdateCategoryModel> categories)
        {
            string url = WebApiDevUrl + "api/voucher/reducecategoryquantity";

            try
            {

                using (var hc = new HttpClient())
                {
                    try
                    {
                        hc.DefaultRequestHeaders.Accept.Clear();
                        hc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        //hc.DefaultRequestHeaders.Authorization =
                        //    new AuthenticationHeaderValue("Bearer", "demo");


                        var content = new StringContent(JsonConvert.SerializeObject(categories), Encoding.UTF8, "application/json");
                        var response = await hc.PutAsync(url, content);
                        if (response.IsSuccessStatusCode)
                        {
                            var responseContent = await response.Content.ReadAsStringAsync();
                            var postOutput = JsonConvert.DeserializeObject<MethodResponse>(responseContent);

                            return postOutput;
                        }
                    }
                    catch (Exception exception)
                    {
                        return new MethodResponse
                        {
                            Status = false,
                            Message = exception.Message
                        };
                    }
                    return new MethodResponse
                    {
                        Status = false,
                        Message = "Error in using method"
                    };
                }
            }
            catch (Exception e)
            {
                return new MethodResponse
                {
                    Status = false,
                    Message = e.Message
                };
            }
        }

        public async Task<List<Sale>> GetAllSales()
        {
            string url = WebApiDevUrl + "api/voucher/getallsales";

            try
            {

                using (var hc = new HttpClient())
                {
                    try
                    {
                        var response = await hc.GetAsync(url);
                        if (response.IsSuccessStatusCode)
                        {
                            var responseContent = await response.Content.ReadAsStringAsync();
                            var postOutput = JsonConvert.DeserializeObject<List<Sale>>(responseContent);

                            return postOutput;
                        }
                    }
                    catch (Exception exception)
                    {
                        return new List<Sale>();
                    }
                    return new List<Sale>();
                }
            }
            catch (Exception e)
            {
                return new List<Sale>();
            }
        }

        public async Task<List<Claims>> GetAllClaims()
        {
            string url = WebApiDevUrl + "api/voucher/getallclaims";

            try
            {

                using (var hc = new HttpClient())
                {
                    try
                    {
                        var response = await hc.GetAsync(url);
                        if (response.IsSuccessStatusCode)
                        {
                            var responseContent = await response.Content.ReadAsStringAsync();
                            var postOutput = JsonConvert.DeserializeObject<List<Claims>>(responseContent);

                            return postOutput;
                        }
                    }
                    catch (Exception exception)
                    {
                        return new List<Claims>();
                    }
                    return new List<Claims>();
                }
            }
            catch (Exception e)
            {
                return new List<Claims>();
            }
        }

        public async Task<MethodResponse> UpdateCategory(Category category)
        {
            string url = WebApiDevUrl + "api/voucher/updatecategory";

            try
            {

                using (var hc = new HttpClient())
                {
                    try
                    {
                        hc.DefaultRequestHeaders.Accept.Clear();
                        hc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        //hc.DefaultRequestHeaders.Authorization =
                        //    new AuthenticationHeaderValue("Bearer", "demo");


                        var content = new StringContent(JsonConvert.SerializeObject(category), Encoding.UTF8, "application/json");
                        var response = await hc.PutAsync(url, content);
                        if (response.IsSuccessStatusCode)
                        {
                            var responseContent = await response.Content.ReadAsStringAsync();
                            var postOutput = JsonConvert.DeserializeObject<MethodResponse>(responseContent);

                            return postOutput;
                        }
                    }
                    catch (Exception exception)
                    {
                        return new MethodResponse
                        {
                            Status = false,
                            Message = exception.Message
                        };
                    }
                    return new MethodResponse
                    {
                        Status = false,
                        Message = "Error in using method"
                    };
                }
            }
            catch (Exception e)
            {
                return new MethodResponse
                {
                    Status = false,
                    Message = e.Message
                };
            }
        }

        public async Task<MethodResponse> UpdateCategoryImage(UpdateCategoryImageModel model)
        {
            string url = WebApiDevUrl + "api/voucher/updatecategoryimage";

            try
            {

                using (var hc = new HttpClient())
                {
                    try
                    {
                        hc.DefaultRequestHeaders.Accept.Clear();
                        hc.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        //hc.DefaultRequestHeaders.Authorization =
                        //    new AuthenticationHeaderValue("Bearer", "demo");


                        var content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                        var response = await hc.PutAsync(url, content);
                        if (response.IsSuccessStatusCode)
                        {
                            var responseContent = await response.Content.ReadAsStringAsync();
                            var postOutput = JsonConvert.DeserializeObject<MethodResponse>(responseContent);

                            return postOutput;
                        }
                    }
                    catch (Exception exception)
                    {
                        return new MethodResponse
                        {
                            Status = false,
                            Message = exception.Message
                        };
                    }
                    return new MethodResponse
                    {
                        Status = false,
                        Message = "Error in using method"
                    };
                }
            }
            catch (Exception e)
            {
                return new MethodResponse
                {
                    Status = false,
                    Message = e.Message
                };
            }
        }
    }
}
