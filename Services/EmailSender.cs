﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SendGrid;
using SendGrid.Helpers.Mail;
using VoucherSystem.Interfaces;

namespace VoucherSystem.Services
{

    public class EmailSender : IEmailSender
    {
        private readonly ILogger<EmailSender> _logger;
        private const string Apikey = "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";

        public EmailSender(ILogger<EmailSender> logger)
        {
            _logger = logger;
        }

        public async Task<bool> SendEmailAsync(string email, string subject, string message)
        {
            try
            {
               
                var client = new SendGridClient(Apikey);

                var msg = new SendGridMessage();

                msg.SetFrom(new EmailAddress("support@heuristixs.com", "Ticket Fort"));

                var recipients = new List<EmailAddress>
                {
                    new EmailAddress(email)
                };

                msg.AddTos(recipients);

                msg.SetSubject(subject);
                msg.AddContent(MimeType.Html, message);

                var res = await client.SendEmailAsync(msg);

                return res.StatusCode == HttpStatusCode.Accepted;

            }
            catch (Exception ex)
            {
                _logger.LogError("Error sending mail", ex.ToString());
                return false;
            }
        }

        public async Task<List<string>> SendMultipleEmailAsync(string subject, string message, List<string> mailList)
        {
            var emailIdList = new List<string>();
            // Plug in your email service here to send an email.
            try
            {
             

                var client = new SendGridClient(Apikey);
                var msg = new SendGridMessage();

                msg.SetFrom(new EmailAddress("ticketfort@heuristixs.com", "Ticket Fort"));

                var recipients = new List<EmailAddress>
                {
                    new EmailAddress("support@heuristixs.com")
                };

                msg.AddTos(recipients);

                foreach (var item in mailList)
                {
                    var person = new Personalization
                    {

                        Subject = subject,
                        Tos = new List<EmailAddress>()
                        {
                            new EmailAddress(item)
                        }

                    };

                    msg.Personalizations.Add(person);
                }

                msg.SetSubject(subject);
                msg.AddContent(MimeType.Html, message);

                var res = await client.SendEmailAsync(msg);             

                if (res.StatusCode != HttpStatusCode.Accepted)
                {
                    _logger.LogError("Error sending mail");
                }

                emailIdList.Add(res.StatusCode.ToString());

              

            }
            catch (Exception ex)
            {
                _logger.LogError("Error sending mail", ex.ToString());

            }
            await Task.FromResult(0);
            return emailIdList;
        }

        public async Task<string> SendEmailWithAttachementAsync(string email, string subject, string message, List<string> files)
        {
            // Plug in your email service here to send an email.

            try
            {
                var client = new SendGridClient(Apikey);

                var msg = new SendGridMessage();

                msg.SetFrom(new EmailAddress("ticketfort@heuristixs.com", "Ticket Fort"));

                var recipients = new List<EmailAddress>
                {
                    new EmailAddress(email)
                };

                msg.AddTos(recipients);

                msg.SetSubject(subject);
                msg.AddContent(MimeType.Html, message);

                var counter = 1;
                foreach (var file in files)
                {
                    var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
                    var pdfBytes1 = htmlToPdf.GeneratePdf(file);

                    msg.AddAttachment("Ticket" + counter + ".pdf", Convert.ToBase64String(pdfBytes1), "application/pdf");
                    counter = counter + 1;
                }


                var res = await client.SendEmailAsync(msg);

                if (res.StatusCode != HttpStatusCode.Accepted)
                {
                    _logger.LogError("Error sending mail");
                }

                await Task.FromResult(0);

                return res.StatusCode.ToString();

            }
            catch (Exception ex)
            {
                _logger.LogError("Error sending mail", ex.ToString());
                return null;
            }

        }

        public async Task<string> SendSupportEmailAsync(string email, string subject, string message)
        {
            try
            {
                var client = new SendGridClient(Apikey);


                var msg = new SendGridMessage();

                msg.SetFrom(new EmailAddress("ticketfort@heuristixs.com", "Ticket Fort"));

                var recipients = new List<EmailAddress>
                {
                    new EmailAddress("support@heuristixs.com")
                };

                msg.AddTos(recipients);

                msg.SetSubject(subject);
                msg.AddContent(MimeType.Html, message);


                var res = await client.SendEmailAsync(msg);

                if (res.StatusCode != HttpStatusCode.Accepted)
                {
                    _logger.LogError("Error sending mail");
                }

                await Task.FromResult(0);

                return res.StatusCode.ToString();

            }
            catch (Exception ex)
            {
                _logger.LogError("Error sending mail", ex.ToString());
                return null;
            }       
        }
    }
}
