﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Azure.ActiveDirectory.GraphClient;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using VoucherSystem.Interfaces;
using VoucherSystem.Models;
using VoucherSystem.ViewModels;

namespace VoucherSystem.Services
{
    public class GraphApiService:IGraphApiService<UserVm>
    {
        private const string TenantName = "ticketfortb2c.onmicrosoft.com";
        private const string AppId = "bacbf0f0-e4ee-4c13-a7e7-d7d144a9a1cf";
        private const string AppSecretKey = "W1kOAXVDyk0Cz1XRjfavLKqF8bsGvYR/jd5QfLIKORc= ";
        private const string AadGraphUri = "https://graph.windows.net";
        private const string Authority = "https://login.microsoftonline.com/ticketfortb2c.onmicrosoft.com";
        public static string AccessToken;

        public ActiveDirectoryClient AadClient;
        private readonly ILogger<GraphApiService> _logger;
        private readonly IEmailSender _emailSender;


        public GraphApiService(ILogger<GraphApiService> logger, IEmailSender emailSender)
        {
            _logger = logger;
            _emailSender = emailSender;


            var authContext = new AuthenticationContext(Authority);
            var clientCredential = new ClientCredential(AppId, AppSecretKey);

            var graphUri = new Uri(AadGraphUri);
            var serviceRoot = new Uri(graphUri, TenantName);
            AadClient = new ActiveDirectoryClient(serviceRoot, async () => await AcquireGraphApiAccessToken(AadGraphUri, authContext, clientCredential));
        }


        private static async Task<string> AcquireGraphApiAccessToken(string graphApiUrl, AuthenticationContext authContext, ClientCredential clientCredential)
        {
            AuthenticationResult result = null;
            var retryCount = 0;
            bool retry;
            do
            {
                retry = false;
                try
                {
                    result = await authContext.AcquireTokenAsync(graphApiUrl, clientCredential);
                    AccessToken = result.AccessToken;
                }
                catch (AdalException ex)
                {
                    if (ex.ErrorCode != "temporarily_unavailable") continue;
                    retry = true;
                    retryCount++;
                    await Task.Delay(3000);
                }
            } while (retry && (retryCount < 3));

            return result?.AccessToken;
        }


        //method for creating company profile
        public async Task<MethodResponse> CompanyRegistration(UserVm entity)
        {
            try
            {
                var u = new User
                {
                    DisplayName = entity.Name,
                    AccountEnabled = true,
                    UserPrincipalName = Regex.Replace(entity.Name, @"\s", "") + "@" + TenantName,
                    TelephoneNumber = entity.Phone,
                    City = "Abuja",
                    CreationType = "LocalAccount",
                    SignInNames = new List<SignInName>
                    {
                        new SignInName
                        {
                            Type = "emailAddress",
                            Value = entity.Email
                        }
                    },
                    PasswordProfile = new PasswordProfile
                    {
                        Password = entity.Password,
                        ForceChangePasswordNextLogin = false
                    }
                };

               await AadClient.Users.AddUserAsync(u);

                //if user creation is succesful then create the company profile here


                return new MethodResponse
                {
                    Status = true,
                    Message = "User Created"
                };
            }
            catch (Exception e)
            {
                _logger.LogError("Error Creating New User");
                //send email for error
                await _emailSender.SendEmailAsync("Support@heuristixs.com", "Error Creating New Company",
                    "There was an error Creating a new user with exception:" + e);

                return new MethodResponse
                {
                    Status = false,
                    Message = e.Message
                };
            }
        }

        public async Task<MethodResponse> UserRegistration(UserVm entity)
        {
            try
            {
                var u = new User
                {
                    DisplayName = entity.Name,
                    AccountEnabled = true,
                    UserPrincipalName = Regex.Replace(entity.Name, @"\s", "") + "@" + TenantName,
                    CreationType = "LocalAccount",
                    SignInNames = new List<SignInName>
                    {
                        new SignInName
                        {
                            Type = "emailAddress",
                            Value = entity.Email
                        }
                    },
                    PasswordProfile = new PasswordProfile
                    {
                        Password = entity.Password,
                        ForceChangePasswordNextLogin = false
                    }
                };

                await AadClient.Users.AddUserAsync(u);

                //if user creation is succesful then create the company profile here
                return new MethodResponse
                {
                    Status = true,
                    Message = "User Created"
                };
            }
            catch (Exception e)
            {
                _logger.LogError("Error Creating New User");
                //send email for error
                await _emailSender.SendEmailAsync("Support@heuristixs.com", "Error Creating New User",
                    "There was an error Creating a new user with exception:" + e);
                return new MethodResponse
                {
                    Status = false,
                    Message = e.Message
                };
            }
        }
    }
}
