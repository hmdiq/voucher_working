﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.Azure.ActiveDirectory.GraphClient;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.IdentityModel.Protocols.OpenIdConnect;
using Microsoft.IdentityModel.Tokens;

namespace VoucherSystem.Extensions
{
    public static class AzureAdB2CAuthenticationBuilderExtensions
    {
        public static AuthenticationBuilder AddAzureAdB2C(this AuthenticationBuilder builder)
            => builder.AddAzureAdB2C(_ => { });

        public static AuthenticationBuilder AddAzureAdB2C(this AuthenticationBuilder builder, Action<AzureAdB2COptions> configureOptions)
        {
            builder.Services.Configure(configureOptions);
            builder.Services.AddSingleton<IConfigureOptions<OpenIdConnectOptions>, ConfigureAzureOptions>();
            builder.AddOpenIdConnect();
            return builder;
        }

        public class ConfigureAzureOptions: IConfigureNamedOptions<OpenIdConnectOptions>
        {
            private readonly AzureAdB2COptions _azureOptions;
            public ActiveDirectoryClient AadClient { get; set; }

            public ConfigureAzureOptions(IOptions<AzureAdB2COptions> azureOptions)
            {
                _azureOptions = azureOptions.Value;
           
                var authContext = new AuthenticationContext("https://login.microsoftonline.com/ticketfortb2c.onmicrosoft.com");
                var clientCredential = new ClientCredential("bacbf0f0-e4ee-4c13-a7e7-d7d144a9a1cf", "W1kOAXVDyk0Cz1XRjfavLKqF8bsGvYR/jd5QfLIKORc= "); //the azure app directory appid and secret key
                const string aadGraphUri = "https://graph.windows.net";
                var graphUri = new Uri(aadGraphUri);
                var serviceRoot = new Uri(graphUri, "ticketfortb2c.onmicrosoft.com");

                AadClient = new ActiveDirectoryClient(serviceRoot, async () => await AcquireGraphApiAccessToken(aadGraphUri, authContext, clientCredential));
            }

            public void Configure(string name, OpenIdConnectOptions options)
            {
                options.ClientId = _azureOptions.ClientId;
                options.Authority = $"{_azureOptions.Instance}/{_azureOptions.Domain}/{_azureOptions.SignUpSignInPolicyId}/v2.0";
                options.UseTokenLifetime = true;
                options.CallbackPath = _azureOptions.CallbackPath;

                options.TokenValidationParameters = new TokenValidationParameters { NameClaimType = "name" };

                options.Events = new OpenIdConnectEvents
                {
                    OnRedirectToIdentityProvider = OnRedirectToIdentityProvider,
                    OnRemoteFailure = OnRemoteFailure,
                    OnTokenValidated = SecurityTokenValidated
                };
            }

            public void Configure(OpenIdConnectOptions options)
            {
                Configure(Options.DefaultName, options);
            }


            private Task OnRedirectToIdentityProvider(RedirectContext context)
            {
                var defaultPolicy = _azureOptions.DefaultPolicy;
                if (!context.Properties.Items.TryGetValue(AzureAdB2COptions.PolicyAuthenticationProperty,
                        out var policy) || policy.Equals(defaultPolicy)) return Task.CompletedTask;
                context.ProtocolMessage.Scope = OpenIdConnectScope.OpenIdProfile;
                context.ProtocolMessage.ResponseType = OpenIdConnectResponseType.IdToken;
                context.ProtocolMessage.IssuerAddress = context.ProtocolMessage.IssuerAddress.ToLower()
                    .Replace($"/{defaultPolicy.ToLower()}/", $"/{policy.ToLower()}/");
                context.Properties.Items.Remove(AzureAdB2COptions.PolicyAuthenticationProperty);
                return Task.CompletedTask;
            }

            private static Task OnRemoteFailure(RemoteFailureContext context)
            {
                context.HandleResponse();
                // Handle the error code that Azure AD B2C throws when trying to reset a password from the login page 
                // because password reset is not supported by a "sign-up or sign-in policy"
                switch (context.Failure)
                {
                    case OpenIdConnectProtocolException _ when context.Failure.Message.Contains("AADB2C90118"):
                        // If the user clicked the reset password link, redirect to the reset password route
                        context.Response.Redirect("/Account/ResetPassword");
                        break;
                    case OpenIdConnectProtocolException _ when context.Failure.Message.Contains("access_denied"):
                        context.Response.Redirect("/");
                        break;
                    default:
                        context.Response.Redirect("/Home/Error");
                        break;
                }
                return Task.CompletedTask;
            }

            private Task SecurityTokenValidated(TokenValidatedContext context)
            {
                return Task.Run(async () =>
                {
                    var oidClaim = context.SecurityToken.Claims.FirstOrDefault(c => c.Type == "oid");

                    if (!string.IsNullOrWhiteSpace(oidClaim?.Value))
                    {
                        var pagedCollection = await AadClient.Users.GetByObjectId(oidClaim.Value).MemberOf.ExecuteAsync();

                        do
                        {
                            var directoryObjects = pagedCollection.CurrentPage.ToList();
                            foreach (var directoryObject in directoryObjects)
                            {
                                var group = directoryObject as Group;

                                if (group != null)
                                {
                                    ((ClaimsIdentity)context.Principal.Identity).AddClaim(new Claim(ClaimTypes.Role, group.DisplayName, ClaimValueTypes.String));
                                }
                            }
                            pagedCollection = pagedCollection.MorePagesAvailable ? await pagedCollection.GetNextPageAsync() : null;
                        }
                        while (pagedCollection != null);
                    }
                });
            }

            private static async Task<string> AcquireGraphApiAccessToken(string graphApiUrl, AuthenticationContext authContext, ClientCredential clientCredential)
            {
                Console.Write("AAd client");
                AuthenticationResult result = null;
                var retryCount = 0;
                bool retry;
                do
                {
                    retry = false;
                    try
                    {
                        result = await authContext.AcquireTokenAsync(graphApiUrl, clientCredential);

                    }
                    catch (AdalException ex)
                    {
                        if (ex.ErrorCode != "temporarily_unavailable") continue;
                        retry = true;
                        retryCount++;
                        await Task.Delay(3000);
                    }
                } while (retry && (retryCount < 3));

                return result?.AccessToken;
            }
        }
    }
}
