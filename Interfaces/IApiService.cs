﻿using System.Collections.Generic;
using System.Threading.Tasks;
using VoucherSystem.Models;

namespace VoucherSystem.Interfaces
{
    public interface IApiService
   {

       Task<MethodResponse> CreateCategory(Category category);

        Task<List<Category>> GetAllCategories();

       Task<MethodResponse> CreateSalesLog(List<Sale> sales);

        Task<Category> GetCategory(int productId);

       Task<MethodResponse> ReduceCategoryQuantity(List<UpdateCategoryModel> models);

       Task<List<Sale>> GetAllSales();

       Task<List<Claims>> GetAllClaims();

       Task<MethodResponse> UpdateCategory(Category category);

       Task<MethodResponse> UpdateCategoryImage(UpdateCategoryImageModel model);
    }
}
