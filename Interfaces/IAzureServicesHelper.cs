﻿using Microsoft.AspNetCore.Http;

namespace VoucherSystem.Interfaces
{
    public interface IAzureServicesHelper
    {
        byte[] GetByteArrayFromImage(IFormFile file);

        string UploadEventImageToCdn(IFormFile imageFile);

        string UploadTicketImageToCdn(IFormFile imageFile);

        string UploadArtistImageToCdn(IFormFile imageFile);
    }
}
