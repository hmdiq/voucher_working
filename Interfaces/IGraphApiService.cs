﻿using System.Threading.Tasks;
using VoucherSystem.Models;

namespace VoucherSystem.Interfaces
{
    public interface IGraphApiService<in T> where T : class
    {
        //for registering company profiles
        Task<MethodResponse> CompanyRegistration(T entity);

        //for registering users
        Task<MethodResponse> UserRegistration(T entity);

    }
}
