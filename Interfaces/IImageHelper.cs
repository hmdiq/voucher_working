﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace VoucherSystem.Interfaces
{
    public interface IImageHelper
    {
        Task<string> UploadCategoryImageToCdnAsync(IFormFile imageFile);
    }
}