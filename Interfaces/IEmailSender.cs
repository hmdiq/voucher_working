﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace VoucherSystem.Interfaces
{
    public interface IEmailSender
    {
        Task<bool> SendEmailAsync(string email, string subject, string message);

        Task<List<string>> SendMultipleEmailAsync(string subject, string message, List<string> mailList);

        Task<string> SendEmailWithAttachementAsync(string email, string subject, string message, List<string> files);

        Task<string> SendSupportEmailAsync(string email, string subject, string message);

    }
}
