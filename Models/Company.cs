﻿namespace VoucherSystem.Models
{
    public class Company 
    {

        public string Name { get; set; }

        public string Email { get; set; }

        public string State { get; set; }

        public string Phone { get; set; }

    }
}
