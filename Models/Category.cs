﻿namespace VoucherSystem.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int Quantity { get; set; }
        
        public int Price { get; set; }

        public string ImageUrl { get; set; }
    }
}
