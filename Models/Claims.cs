﻿namespace VoucherSystem.Models
{
    public class Claims
    {
        public string Email { get; set; }

        public string ClaimTitle { get; set; }

        public string ClaimDescription { get; set; }
    }
}
