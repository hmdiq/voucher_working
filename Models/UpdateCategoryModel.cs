﻿namespace VoucherSystem.Models
{
    public class UpdateCategoryModel
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
    }
}
