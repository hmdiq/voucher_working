﻿namespace VoucherSystem.Models
{
    public class PrinterObject
    {
        public int Id { get; set; }

        public int Quantity { get; set; }

        public string Product { get; set; }

        public int Price { get; set; }
    }
}
