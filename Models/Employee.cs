﻿namespace VoucherSystem.Models
{
    public class Employee 
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string Password { get; set; }

        public Company Company { get; set; }

        public bool IsActive { get; set; }

    }
}
