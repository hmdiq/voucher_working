﻿using System;

namespace VoucherSystem.Models
{
    public class Sale
    {
        public string Category { get; set; }

        public int CategoryId { get; set; }

        public string TimeStamp { get; set; }

        public string SalesPerson { get; set; }

        public int Quantity { get; set; }

        public int SalesTotal { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ModifiedOn { get; set; }
    }
}
