﻿namespace VoucherSystem.Models
{
    public class MethodResponse
    {
        public bool Status { get; set; }

        public string Message { get; set; }
    }
}
