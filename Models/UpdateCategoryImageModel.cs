﻿namespace VoucherSystem.Models
{
    public class UpdateCategoryImageModel
    {
        public int Id { get; set; }
        public string ImgUrl { get; set; }
    }
}
